# eza-wrapper

Simple wrapper for eza(exa) to add the `--icons` switch
whenever the `--long` switch is active. It recognizes
the short form `-l` and the merged short form.

To install
```sh
sudo make install
```
