.PHONY: default
default: build

.PHONY: run
run: src/main.rs
	cargo run

.PHONY: build
build: src/main.rs
	cargo build

target/release/eza-wrapper: src/main.rs
	cargo build --release

install: target/release/eza-wrapper
	cp target/release/eza-wrapper /usr/bin/eza-wrapper
	chown root:root /usr/bin/eza-wrapper
	chmod 755 /usr/bin/eza-wrapper
