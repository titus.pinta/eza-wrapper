// Copyright (C) 2024 Titus Pinta
// SPDX-License-Identifier: GPL-3.0-or-later
// Part of https://gitlab.com/titus.pinta/config/
// See LICENSE.md for the license

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::env;
use std::process::Command;

fn check_switch(s: &String) -> bool {
    let mut chs = s.chars();
    return match chs.next() {
        Some('-') => match chs.next() {
            Some('-') => s == "--long",
            Some(_) => s.find('l').is_some(),
            _ => false,
        },
        _ => false,
    }
}

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();
    let mut cmd = Command::new("exa");
    if args.iter().any(check_switch) {
        cmd.arg("--icons");
    }
    cmd.args(args)
        .spawn()
        .expect("exa failed to start")
        .wait()
        .expect("exa failed to start");
}
